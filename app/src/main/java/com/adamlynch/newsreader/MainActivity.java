
/*
 * Copyright 2020, Adam Glick-Lynch, All rights reserved.
 */

package com.adamlynch.newsreader;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/*
Opens up a list of the top popular articles on hacker news
When you select an item open article in another activity
Instead of using a database I will just use arrays as I will not
be permantly storing all of the html of each web page.
 */


public class MainActivity extends AppCompatActivity
{
    int numberOfWebsites = 10;
    ArrayList<Integer> arrayWebsiteIds;
    ArrayList<String> arrayWebsiteTitles;
    ArrayList<String> arrayWebsiteURLS;

    public class DownloadTask extends AsyncTask<String, Void, String>
    {

        @Override
        protected String doInBackground(String... urls)
        {
            getInitialWebsiteIds(urls);
            getWebsiteHTML();
            return "";
        }

        //Code runs after doInBackground task finishes
        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);
            //Fill list view information after everything has been added to array
            fillListViewInformation();
        }
    }

    public void fillListViewInformation()
    {
        ListView listView = findViewById(R.id.listView);
        //Create/Link arrayadapter
        ArrayAdapter<String> arrayAdapter =
                new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,arrayWebsiteTitles);
        listView.setAdapter(arrayAdapter);
        //Create listner for when items are selected
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            //Open the website of selected title in new activity
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Intent intent = new Intent(getApplicationContext(), WebsiteActivity.class);
                intent.putExtra("url", arrayWebsiteURLS.get(position));
                startActivity(intent);
            }
        });
    }

    public void getInitialWebsiteIds(String... urls)
    {
        String result = "";
        URL url;
        HttpURLConnection urlConnection = null;

        try {
            //setting string to url
            url = new URL(urls[0]);
            //setting connection to url
            urlConnection = (HttpURLConnection) url.openConnection();
            //creating input stream
            InputStream in = urlConnection.getInputStream();
            //Creating stream reader
            InputStreamReader reader = new InputStreamReader(in);
            //Reader returns int
            int data = reader.read();

            //While loops until no more data is left to read
            while (data != -1) {
                //Convert the integer to a character
                char current = (char) data;
                //Add character to the result string
                result += current;
                //Moves onto next character every time read is called
                data = reader.read();
            }
            //Adds the web ids to an array
            getWebIds(result);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getWebsiteHTML() {

        try {
            for(int i = 0; i<arrayWebsiteIds.size(); i++)
            {
                String result = "";
                URL url;
                HttpURLConnection urlConnection = null;

                String middle = arrayWebsiteIds.get(i).toString();

                String start = "https://hacker-news.firebaseio.com/v0/item/";
                String finish = ".json?print=pretty";
                //setting string to url
                url = new URL(start+middle+finish);
                //setting connection to url
                urlConnection = (HttpURLConnection) url.openConnection();
                //creating input stream
                InputStream in = urlConnection.getInputStream();
                //Creating stream reader
                InputStreamReader reader = new InputStreamReader(in);
                //Reader returns int
                int data = reader.read();

                //While loops until no more data is left to read
                while (data != -1)
                {
                    //Convert the integer to a character
                    char current = (char) data;
                    //Add character to the result string
                    result += current;
                    //Moves onto next character every time read is called
                    data = reader.read();
                }
                //Parse html for title and add to array
                getWebTitles(result);
                //Parse html for url and add to array
                getWebURLS(result);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        arrayWebsiteIds = new ArrayList<>();
        arrayWebsiteTitles = new ArrayList<>();
        arrayWebsiteURLS = new ArrayList<>();
        DownloadTask task = new DownloadTask();
        try {
            task.execute("https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty").get();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    //Add website IDs to array
    public void getWebIds(String s)
    {
        try {
            JSONArray arr = new JSONArray(s);
            for(int i =0; i < numberOfWebsites; i++) {
                arrayWebsiteIds.add(arr.getInt(i));
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    //Get the JSON info for the title of each webpage
    public void getWebTitles(String s)
    {
        try {
            JSONObject jsonObject = new JSONObject(s);
            String title = jsonObject.getString("title");
            arrayWebsiteTitles.add(title);
            //Add titles to title array
            //Add urls to url array
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
    public void getWebURLS(String s)
    {
        try {
            JSONObject jsonObject = new JSONObject(s);
            String url = jsonObject.getString("url");
            arrayWebsiteURLS.add(url);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
}
