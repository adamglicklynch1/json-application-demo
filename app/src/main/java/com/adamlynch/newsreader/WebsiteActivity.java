
/*
 * Copyright 2020, Adam Glick-Lynch, All rights reserved.
 */

package com.adamlynch.newsreader;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebsiteActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_website);

        WebView webView = findViewById(R.id.webView);
        //Enable or disable javascript
        webView.getSettings().setJavaScriptEnabled(true);
        //Sets the browser to open in the webview
        webView.setWebViewClient(new WebViewClient());
        //Set the webpage you want opened
        webView.loadUrl(getIntent().getStringExtra("url"));
    }
}
